package rockPaperScissors;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String human_choice = humanChoice().toLowerCase();
            String computer_choice = randomChoice().toLowerCase();
            String choice_string=String.format("Human chose %s, computer chose %s.", human_choice, computer_choice);

            if (isWinner(human_choice, computer_choice)) {
                System.out.println(choice_string + " Human wins!");
                humanScore += 1;
            }
            else if (isWinner(computer_choice, human_choice)) {
                System.out.println(choice_string + " Computer wins!");
                computerScore += 1;
            }
            else {
                System.out.println(choice_string + " Its a tie!");
            }
            String score_string=String.format("Score: human %s, computer %s", humanScore, computerScore);
            System.out.println(score_string);
            roundCounter += 1;

            String continue_playing = continuePlaying().toLowerCase();
            if (continue_playing.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

        } 
    }

    public String randomChoice() {
        int randomNum = (int)(Math.random() * 3);
        return rpsChoices.get(randomNum);
    }

    public String humanChoice() {
        while (true) {
            String answer = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validInput(answer.toLowerCase(), rpsChoices)) {
                return answer;
            }
            else {
                String sf1=String.format("I do not understand %s. Could you try again?", answer); 
                System.out.println(sf1);
            }
        }

    }

    public Boolean validInput(String input, List<String> list) {
        return (list.contains(input));
    }

    public String continuePlaying() {
        while (true) {
            String answer = readInput("Do you wish to continue playing? (y/n)?");
            List<String> yn = Arrays.asList("y", "n");

            if (validInput(answer.toLowerCase(), yn)){
                return answer;
            }
            else {
                String sf1=String.format("I dont understand %s. Try again", answer); 
                System.out.println(sf1);

            }
        }
    }
    public static Boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("rock") && choice2.equals("scissors")) {
            return true;
        }
        else if (choice1.equals("rock") && choice2.equals("paper")) {
            return false;
        }
        else if (choice1.equals("scissors") && choice2.equals("paper")) {
            return true;
        }
        else if (choice1.equals("scissors") && choice2.equals("rock")) {
            return false;
        }
        else if (choice1.equals("paper") && choice2.equals("rock")) {
            return true;
        }
        else if (choice1.equals("paper") && choice2.equals("scissors")) {
            return false;
        }
        else {
            return false;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
